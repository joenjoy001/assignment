package com.stackroute.basics;

import java.util.Scanner;

public class SortAscNumber {

	public static void main(String[] args) {
		new SortAscNumber().getNumbers();
	}

	// get the numbers from user through console
	public void getNumbers() {
		Scanner sc = new Scanner(System.in);
		int number1 = sc.nextInt();
		int number2 = sc.nextInt();
		int number3 = sc.nextInt();
		int number4 = sc.nextInt();
		new SortAscNumber().numberSorter(number1, number2, number3, number4);

	}

	// logic to sort the numbers
	public String numberSorter(int number1, int number2, int number3, int number4){
    	    	int low1,high1,low2,high2,lowest,middle1,middle2,highest;
    	    if(number1 < number2) {
    	        low1 = number1;
    	        high1 = number2;
    	    }
    	    else {
    	        low1 = number2;
    	        high1 = number1;
    	    }

    	    if(number3 < number4) {
    	        low2 = number3;
    	        high2 = number4;
    	    }
    	    else {
    	        low2 = number4;
    	        high2 = number3;
    	    }

    	    if(low1 < low2) {
    	        lowest = low1;
    	        middle1 = low2;
    	    }
    	    else {
    	        lowest = low2;
    	        middle1 = low1;
    	    }

    	    if (high1 > high2) {
    	        highest = high1;
    	        middle2 = high2;
    	    }
    	    else {
    	        highest = high2;
    	        middle2 = high1;
    	    }

    	    if(middle1 < middle2) {
    	    	number1 = lowest;
    	    	number2 =middle1;
    	    	number3 = middle2;
    	    	number4 =highest;
    	        //return (lowest,middle1,middle2,highest);
    	    }
    	    else {
    	    	number1 = lowest;
    	    	number2 =middle2;
    	    	number3 = middle1;
    	    	number4 =highest;
    	        //return (lowest,middle2,middle1,highest);
    	    	}
    	    	//sort number -return as string 
    	    	String result = "Sorted:{"+number1+","+number2+","+number3+","+number4+"}";
    	    	System.out.println(result);
    	    	
		/*for(int i=0;i<=array.length-1;i++) {
			for(int j=0;j<array.length;j++) {
				if(array[i]<array[j]) {
					
					int temp=array[i];
					array[i]=array[j];
					array[j]=temp;
				}
			}*/

		
			//System.out.println("Sorted:{"+array[0]+","+array[1]+","+array[2]+","+array[3]+"}");
			 //Sorted:{12,13,14,15}
			

			
		
		return result;
	}
	
}
