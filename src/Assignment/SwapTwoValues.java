package Assignment;

import java.util.Scanner;

public class SwapTwoValues {

	public static void main(String[] args) {
		System.out.println("please enter the value of a : ");
		int a,b;
		Scanner sc=new Scanner(System.in);
		a=sc.nextInt();
		System.out.println("please enter the value of b : ");
		b=sc.nextInt();
		System.out.println("Before Swapping value of a is "+a+" and value of b is "+b);
		a=a+b;
		b=a-b;
		a=a-b;
		System.out.println("After Swapping value of a is "+a+" and value of b is "+b);
		sc.close();
		

	}

}
