package Assignment;

public class StringPalindromeOrNot {

	public static void main(String[] args) {
		String firstName="malayalam";
		char[] name=firstName.toCharArray();
		
		for(char w:name) {
			//System.out.println(w);
		}
		
		//System.out.println("size of an character array is "+name.length);
		//System.out.println("string in reverse order is ");
		for(int i=name.length-1;i>=0;i--) {
			//System.out.println(name[i]);
		}
		String stringValue="";
		for(int i=name.length-1;i>=0;i--) {
			stringValue=stringValue.concat(String.valueOf(name[i]));
		}
		//System.out.println("concatenated string is "+stringValue);
		
		if(firstName.equals(stringValue)) {
			System.out.println(firstName+" is palindrome");
		}else {
			System.out.println(firstName+" is not palindrome");
		}

	}

}
